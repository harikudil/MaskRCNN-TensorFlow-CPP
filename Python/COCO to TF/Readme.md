# Convert COCO based json file into Tensor Flow TF format.
* Save the annotated image as via_export_coco.json using VIA image annotation software
* Run coco_to_TFformat.py with following command line
```
python coco_to_TFrecord.py 
--train_image_dir = Path_containing_train_images 
--train_annotations_file = path_containing_coco_json_file/via_export_coco.json 
--output_dir = Path_to_save_the_TFrecord_file 
--shards = 1

```
